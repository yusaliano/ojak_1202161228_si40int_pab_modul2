package com.example.ojak_1202161228_si40int_pab_modul2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class CheckoutActivity extends AppCompatActivity {
    TextView tvTanggalBerangkat, tvTanggalPulang, tvJumlahTiket, tvHargaTotal, tvTujuan;
    Button btnKonfirmasi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        tvTujuan = findViewById(R.id.tv_tujuan_summary);
        tvTanggalBerangkat = findViewById(R.id.tv_tanggal_berangkat_summary);
        tvTanggalPulang = findViewById(R.id.tv_tanggal_pulang_summary);
        tvJumlahTiket = findViewById(R.id.tv_jumlah_tiket_summary);
        tvHargaTotal = findViewById(R.id.tv_harga_total_summary);
        btnKonfirmasi = findViewById(R.id.btn_konfirmasi);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String tujuan, tanggalBerangkat, tanggalPulang, jumlahTiket, hargaTotal;
            tujuan = extras.getString("tujuan");
            tanggalBerangkat = extras.getString("tanggalBerangkat");
            tanggalPulang = extras.getString("tanggalPulang");
            jumlahTiket = extras.getString("jumlahTiket");
            hargaTotal = extras.getString("hargaTotal");
            tvTujuan.setText(tujuan);
            tvTanggalBerangkat.setText(tanggalBerangkat);
            tvTanggalPulang.setText(tanggalPulang);
            tvJumlahTiket.setText(jumlahTiket);
            tvHargaTotal.setText(hargaTotal);
        }

        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(CheckoutActivity.this, MainActivity.class);
                finish();
            }
        });


    }

    //    Start Log Activity LifeCycle
    @Override
    protected void onStart() {
        super.onStart();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onStart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("PAB/" + this.getClass().getSimpleName(), "ini onPause");
    }

//    End Log Activity LifeCycle
}

